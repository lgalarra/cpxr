This is an implementation of CPXR algorithm described in following paper:

> G. Dong and V. Taslimitehrani, **"Pattern-Aided Regression Modeling and Prediction Model Analysis,"** in IEEE Transactions on Knowledge and Data Engineering, vol. 27, no. 9, pp. 2452-2465, Sept. 1 2015. doi: [10.1109/TKDE.2015.2411609](http://ieeexplore.ieee.org/document/7058431/)


Here a small example about how to use the algorithm

```python
from cpxr import CPXR
from cpxr import loadCSVData

trainingDataPath = '/some/data/path'
testingDataPath = '/some/other/data/path'
cpxrLearner = CPXR()
trainingData = loadCSVData(trainingDataPath)
cpxrModel = cpxrLearner.fit1(trainingData, 'target-variable-name')
Y = cpxrModel.predict(loadCSVData(testingDataPath))
```

Please see the file tests/test_cpxr.py and the main routine in cpxr.py for more examples. The method loadCSVData accepts csv data with typed headers, i.e., column-name@type. We recommend type=string and type=numeric (which translates to float), but any Python type is supported (e.g., int, float, etc.).

