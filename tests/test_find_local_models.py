'''
Created on Apr 20, 2018

@author: lgalarra
'''
import unittest
import math
import pandas as pd
from find_local_regression_models import PatternBasedRegression
from sklearn import linear_model
import numpy as np



class Test(unittest.TestCase):

    def testMatchesDataPoint(self):
        exampleDataset = pd.DataFrame({'nat' : ['ec', 'ec', 'fr'  , 'fr', 'fr', 'ec', 'fr', 'ec'], 
                                       'prof': ['cr', 'st', 'pf'  , 'pf', 'pf', 'pf', 'pf',  'pf'], 
                                       'exp' : ['l' , 'l' , 'm'   , 'h' ,  'h', 'h' , 'l' ,  'h'],
                                       'hob' : ['ck', 'tv', 't'   , 'ms', 'ms', 'tv', 'ms',  't'],
                                       'age' : [20.0, 25.0, 30.0 ,  32.0 , 33.0, 39.0, 21.0, 45.0],                                                                              
                                       'sal' : [800 , 1500, 3000,  3200,  3300 , 1500, 2100, 1550],
                                       'perm': [ '1', '0' , '1' ,  '1' , '0'  , '1' , '0' ,  '1']
                                       })
        pbr = PatternBasedRegression(regressionMethod=linear_model.LinearRegression, 
                 overlapTolerance=0.5, errorTolerance=0.45, minSupport=2,
                 minDiscriminativeRatio=1, enableDiscretization=False, 
                 forceDiscretization=False)
 
        models = pbr.modelsAsDict(pbr.fitWithoutFiltering(exampleDataset, 'sal'))
        localModel1 = models[(('nat', 'fr'), ('prof', 'pf'))]
        localModel2 = models[(('perm', '1'),)]
        assert not localModel1.matches(exampleDataset.iloc[0])
        assert not localModel1.matches(exampleDataset.iloc[1])
        assert localModel2.matches(exampleDataset.iloc[0])
        assert not localModel2.matches(exampleDataset.iloc[1])        
        assert localModel1.matches(exampleDataset.iloc[2])
        assert localModel1.matches(exampleDataset.iloc[3])        
        assert not localModel1.matches(exampleDataset.iloc[5])
        assert localModel2.matches(exampleDataset.iloc[5])        
        assert not localModel2.matches(exampleDataset.iloc[6])        

    def testPatternBasedRegression(self):
        #np.random.seed(1235)
        exampleDataset = pd.DataFrame({'nat' : ['ec', 'ec', 'fr'  , 'fr', 'fr', 'ec', 'fr', 'ec'], 
                                       'prof': ['cr', 'st', 'pf'  , 'pf', 'pf', 'pf', 'pf',  'pf'], 
                                       'exp' : ['l' , 'l' , 'm'   , 'h' ,  'h', 'h' , 'l' ,  'h'],
                                       'hob' : ['ck', 'tv', 't'   , 'ms', 'ms', 'tv', 'ms',  't'],
                                       'age' : [20.0, 25.0, 30.0 ,  32.0 , 33.0, 39.0, 21.0, 45.0],                                                                              
                                       'sal' : [800 , 1500, 3000,  3200,  3300 , 1500, 2100, 1550],
                                       'perm': [ '1', '0' , '1' ,  '1' , '0'  , '1' , '0' ,  '1']
                                       })
        pbr = PatternBasedRegression(regressionMethod=linear_model.LinearRegression, 
                 overlapTolerance=0.5, errorTolerance=0.45, minSupport=2,
                 minDiscriminativeRatio=1, enableDiscretization=False, 
                 forceDiscretization=False)
 
        models = pbr.modelsAsDict(pbr.fitWithoutFiltering(exampleDataset, 'sal'))  
        localModel1 = models[(('nat', 'fr'), ('prof', 'pf'))]
        localModel2 = models[(('perm', '1'),)]
        intersection = 2.0
        union = 7.0
        assert math.isclose(pbr.overlap(localModel1, localModel2), intersection / union) 
 

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testPatternBasedRegression']
    unittest.main()