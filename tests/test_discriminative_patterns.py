'''
Created on Apr 10, 2018

@author: lgalarra
'''
import unittest
import pandas as pd
import mine_discriminative_patterns as dpm
import time


class TestDiscriminativePatternsMiner(unittest.TestCase):

 
    def testClosedPatterns(self):
        print('Running testClosedPatterns')
        exampleDataset = pd.DataFrame({'nat' : ['ec', 'ec', 'fr', 'fr', 'gr'], 
                                       'prof': ['cr', 'cr', 'pf', 'pf', 'dr'], 
                                       'expi' : ['l' , 'h' , 'm' , 'h' ,  'h'],
                                       'perm' : [1, 0, 1, 0, 1]
                                       })
        miner = dpm.DiscriminativePatternsMiner(exampleDataset, 'perm')
        closedPatterns = miner.mineClosedPatterns(2)
        print('testClosedPatterns:', closedPatterns)
        assert ((('nat', 'ec'), ('prof', 'cr')), 2) in closedPatterns
        assert ((('nat', 'fr'), ('prof', 'pf')), 2 )in closedPatterns
        assert ((('expi', 'h'),), 3) in closedPatterns
        assert len(closedPatterns) == 4
             
        
    def testDiscriminativePatterns(self):
        print('Running testDiscriminativePatterns')
        exampleDataset = pd.DataFrame({'nat' : ['ec', 'ec', 'fr', 'fr', 'gr', 'gr', 'gr', 'fr'], 
                                       'prof': ['cr', 'cr', 'pf', 'pf', 'dr', 'ph', 'ph', 'pf'], 
                                       'expi' : ['l' , 'h' , 'm' , 'h' ,  'h', 'm' , 'l' , 'l'],
                                       'dom' : ['pm', 'ml', 'pm', 'pm', 'db', 'db', 'db', 'sw'],
                                       'perm': [  1 ,  0  ,  1  ,  1  ,  0  ,  0  ,  1  ,  0]
                                       })
        miner = dpm.DiscriminativePatternsMiner(exampleDataset, 'perm')
        closedPatterns = miner.mineClosedPatterns(3)
        assert ((('nat', 'ec'), ('prof', 'cr')), 2) not in closedPatterns
        assert ((('nat', 'fr'), ('prof', 'pf')), 3) in closedPatterns
        discriminativePatterns = miner.mineDiscriminativePatterns(closedPatterns, 2.0, dpm.Metric.RELATIVE_RISK , [0, 1])
        print('testDiscriminativePatterns(relative-risk):', discriminativePatterns)
        assert ((('dom', 'db'), ('nat', 'gr')), (2.0, 3)) in discriminativePatterns
        discriminativePatterns = miner.mineDiscriminativePatterns(closedPatterns, 2.0, dpm.Metric.ODDS_RATIO, [0, 1])
        print('testDiscriminativePatterns(odds-ratio):', discriminativePatterns)
        assert ((('dom', 'db'), ('nat', 'gr')), (3.0, 3)) in discriminativePatterns
    
    
    def testClosedPatterns2(self):
        print('Running testClosedPatterns2')
        exampleDataset = pd.DataFrame({'nat' : ['ec', 'ec', 'fr', 'fr', 'fr'], 
                                       'prof': ['cr', 'cr', 'pf', 'pf', 'pf'], 
                                       'expi' : ['l' , 'h' , 'm' , 'h' ,  'h'],
                                       'perm' : [1, 0, 1, 0, 1]
                                       })
        miner = dpm.DiscriminativePatternsMiner(exampleDataset, 'perm')
        closedPatterns = miner.mineClosedPatterns(2)
        print('testClosedPatterns2:', closedPatterns)
        assert ((('nat', 'ec'), ('prof', 'cr')), 2) in closedPatterns
        assert ((('nat', 'fr'), ('prof', 'pf')), 3) in closedPatterns
        assert ((('expi', 'h'), ('nat', 'fr'), ('prof', 'pf')), 2) in closedPatterns        
        assert ((('expi', 'h'),), 3) in closedPatterns
  
    def testClosedPatterns3(self):
        print('Running testClosedPatterns3')
        exampleDataset = pd.DataFrame({'nat' : ['ec', 'ec', 'fr', 'fr', 'fr'], 
                                       'prof': ['cr', 'st', 'pf', 'pf', 'pf'], 
                                       'expi' : ['l' , 'h' , 'm' , 'h' ,  'l'],
                                       'hob' : ['ck', 'tv', 't' , 't' , 'ms'],
                                       'age' : [ 20 ,  25 ,  30 ,  32 , 33],                                                                              
                                       'perm' : [1, 0, 1, 0, 1]
                                       })
        miner = dpm.DiscriminativePatternsMiner(exampleDataset, 'perm')
        closedPatterns = miner.mineClosedPatterns(2)
        print('testClosedPatterns3:', closedPatterns)
        assert(True)
            
             
#     def testClosedPatternsWineData(self):
#         print('Running testClosedPatternsWineData')
#         data = pd.read_csv('data/bsv_vigne_maladies_epicure_only_symbolic2.csv')
#         print(len(data), 'records read')
#         miner = dpm.DiscriminativePatternsMiner(data, 'month')
#         startTime = time.process_time()
#         closedPatterns = miner.mineClosedPatterns(100)
#         print('Mine time (support=100)', time.process_time() - startTime, 'seconds')        
#         assert len(closedPatterns) > 1
#         print('testWineData:', closedPatterns[:5])        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()