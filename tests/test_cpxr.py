'''
Created on Apr 24, 2018

@author: lgalarra
'''
import unittest
import pandas as pd
import numpy as np
from math import sqrt
from cpxr import CPXR
from sklearn import linear_model
from sklearn.metrics.regression import mean_squared_error

class DummyLinearRegression(object):
    
    def __init__(self):
        self.coef_ = np.array([-0.29, 2.32, 0.5, 7.19])
        self.intercept_ = -74.07
    
    def fit(self, X, y):
        return self

    def predict(self, X):
        return X.dot(self.coef_) + self.intercept_
            

class Test(unittest.TestCase):


    def testExampleFromPaper(self):
        data = pd.read_csv(open('data/example_paper_cpxr.csv', 'r'), sep=' ')
        cpxr = CPXR(regressionMethod=linear_model.LinearRegression, defaultRegressionFunction=DummyLinearRegression())
        cpxrFn = cpxr.fit1(data, 'y')
        assert len(cpxrFn.localModels) > 0
        y = data['y']
        print(cpxrFn)        
        rmseCPXR = sqrt(mean_squared_error(y, cpxrFn.predict(cpxrFn.getDataUsedForLearning())))
        dlr = DummyLinearRegression()
        rmseBase = sqrt(mean_squared_error(y, dlr.predict(cpxrFn.getNumericalDataUsedForLearning())))
        reduction = (rmseBase - rmseCPXR) / rmseBase 
        print('Reduction in RMSE w.r.t base regression model: ', reduction)
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testMatches']
    unittest.main()