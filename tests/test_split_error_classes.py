'''
Created on Mar 20, 2018

@author: lgalarra
'''
import unittest
import split_error_classes as split
import pandas as pd
from sklearn import linear_model


class Test(unittest.TestCase):


    def testSplit(self):
        dataFrame = pd.DataFrame({'age':     [30,    25,  15,    50],
                                  'weight' : [78,    65,  120,   83], 
                                  'salary':  [3500, 3000, 1800, 4600]})
        
        splitter = split.ErrorBasedSplitter(dataFrame, 0.5)
        le, se, rm = splitter.split('salary', linear_model.LinearRegression())
        assert len(le) == 1
        assert len(se) == 3


if __name__ == "__main__":
    import sys;sys.argv = ['', 'Test.testName']
    unittest.main()