'''
Created on Mar 22, 2018

@author: lgalarra
'''
import unittest
import pandas as pd
#from lib.discretization import MDLP
from mdlp.discretization import MDLP
from sklearn import linear_model
from sklearn.datasets import load_iris
from .data import load_salary_small_dataset
import split_error_classes as split


class TestMDLP(unittest.TestCase):


    def testIntegration(self):
        iris = load_iris()
        X = iris.data
        y = iris.target
        mdlp = MDLP()
        conv_X = mdlp.fit_transform(X, y)
        intervals = mdlp.cat2intervals(conv_X, 0)  
        print('testIntegration') 
        print(intervals)
        assert True
        
    def testSimpleDiscretization(self):
        data = load_salary_small_dataset()
        splitter = split.ErrorBasedSplitter(data, 0.75)
        le, se, rm = splitter.split('salary', linear_model.LinearRegression())
        le['salary_le'] = pd.Series(1, index=le.index)
        se['salary_le'] = pd.Series(0, index=se.index)
        X = pd.concat([le, se]) 
        y = X['salary_le']
        del X['salary_le']
        
        mdlp = MDLP()
        conv_X = mdlp.fit_transform(X, y)
        intervals = mdlp.cat2intervals(conv_X, 2)
        print('testSimpleDiscretization')
        print(intervals)
        assert True

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'TestMDPL.testSimpleDiscretization']
    unittest.main()