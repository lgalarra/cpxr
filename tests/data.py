'''
Created on Mar 23, 2018

@author: lgalarra
'''

import pandas as pd

salary_small_dataset = pd.DataFrame({'age': [30, 25, 15, 50],
                                  'weight' : [78, 65, 120, 83], 
                                  'salary': [3500, 3000, 1800, 4600]})

def load_salary_small_dataset() :
    return salary_small_dataset.copy() 