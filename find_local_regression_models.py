'''
Created on Apr 17, 2018

@author: lgalarra
'''
import re
import pandas as pd
import numpy as np
from mdlp.discretization import MDLP
from math import sqrt
from math import nan
from math import isnan
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
from split_error_classes import ErrorBasedSplitter
from mine_discriminative_patterns import DiscriminativePatternsMiner
from mine_discriminative_patterns import Metric
from mine_discriminative_patterns import queryDataset


def root_mean_squared_error(yTrue, yPredicted):
    return sqrt(mean_squared_error(yTrue, yPredicted))


def regressionModelToStr(data, regressionModel): 
    result = ''
    columns = data.columns
    for i in range(len(columns)) :            
        result += ("%.2f" % regressionModel.coef_[i])
        result += '*' + columns[i] + ' + '
    
    result = result + str(regressionModel.intercept_)
    return result 

def discretizeValue(value, parsedIntervals, discrVals):
    for i in range(len(parsedIntervals)) :
        if value > parsedIntervals[i][0] and value <= parsedIntervals[i][1] :
            return discrVals[i]  

    return None    

class PatternBasedRegressionFunction(object):
    
    '''
    It defines the return object of the CPXR algorithm. It defines a regression
    function that makes predictions based on a default regression model and a set of
    local models that apply to subsets of the data. 
    '''
    
    def __init__(self, localModels, referenceRegressionModel):
        self.localModels = localModels
        self.referenceRegressionModel = referenceRegressionModel
        self.weights = [model.getARR() for model in self.localModels]
        self.models2Weights = {model.pattern[0]: model.getARR() for model in self.localModels}        
        self.defaultRegressionModel = None
        self.data2Models, self.models2Data, self.modelsDict = self.indexRowsAndPatterns()
    
    def indexRowsAndPatterns(self):
        index2PatternIdx = {}
        pattern2IndexIdx = {}
        modelsDict = {}
        
        for localModel in self.localModels :
            pattern2IndexIdx[localModel.pattern[0]] = []
            modelsDict[localModel.pattern[0]] = localModel
        
        pattern2IndexIdx['default'] = []
        
        for index, row in self.getDataUsedForLearning().iterrows():
            index2PatternIdx[index] = []
            for model in self.localModels :
                if model.matches(row) :
                    index2PatternIdx[index].append(model)
                    pattern2IndexIdx[model.pattern[0]].append(index)
        
        # Keep track of the uncovered rows
        for index in index2PatternIdx :
            if len(index2PatternIdx[index]) == 0:
                pattern2IndexIdx['default'].append(index)
                
        
        return index2PatternIdx, pattern2IndexIdx, modelsDict    

    
    def getReferenceRegressionFunction(self):
        return self.referenceRegressionModel
    
    def getNumericalDataUsedForLearning(self):
        return self.localModels[0].originalData[self.localModels[0].numericalHeadersForPrediction]
    
    def getDataUsedForLearning(self):
        return self.localModels[0].originalData
    
    def getModels2RowIndex(self):
        return self.models2Data
    
    def getModelsDictionary(self):
        return self.modelsDict
    
    def getUncoveredData(self, X, y):
        '''
        It returns the data points that are not covered by any of the local 
        patterns.
        '''
        newX = []
        newY = []
        copyX = X.copy()
        for index, row in copyX.iterrows():
            if index in self.data2Models :
                if len(self.data2Models[index]) == 0 :
                    newX.append(row)
                    newY.append(y.loc[index])                
        
        #print(len(newX), 'uncovered items') 
        return pd.DataFrame(newX), pd.Series(newY)
    
    def computeLocalData(self, X, y):
        '''
        It returns the data points that covered by at least one local model.
        '''
        newX = []
        newY = []
        for index, row in X.iterrows():
            if index in self.data2Models :
                if len(self.data2Models[index]) > 0 : 
                    newX.append(row)
                    newY.append(y.loc[index])
         
        return pd.DataFrame(newX), pd.Series(newY)
    
    def getTRR(self, X, y):       
        numVars = self.localModels[0].numericalHeadersForPrediction
        numX = X[numVars]
        yDefaultModel = self.referenceRegressionModel.predict(numX)
        residualsDefaultModel = np.sum(np.abs(yDefaultModel - y))
        
        localX, localY = self.computeLocalData(X, y)
        
        predictedDefaultLocalY = self.referenceRegressionModel.predict(localX[numVars])
        residualsDefaultModelOnLocalData = np.sum(np.abs(predictedDefaultLocalY - localY))
        
        #predictedLocalY = self.predictWithIndexHelp(localX)
        predictedLocalY = self.predictWithIndexHelpImproved(localX)
        residualsFunctionLocalData = np.sum(np.abs(predictedLocalY - localY))
        
        return (residualsDefaultModelOnLocalData - residualsFunctionLocalData) / residualsDefaultModel
        
    
    def targetMetricImprovementWithIndexHelp(self, X, y, targetMetric=root_mean_squared_error):
        '''
        It calculates the difference in the target metric of 
        the whole pattern-aided model and the default regression model.
        '''
        ## Predict using the local patterns + the default model
        numericalX = X[self.localModels[0].numericalHeadersForPrediction]
        #yModel = self.predictWithIndexHelp(X)       
        yModel = self.predictWithIndexHelpImproved(X)       
                
        accuracyModel = targetMetric(y, yModel)        
        ## Predict using only the default model
        yDefault = self.referenceRegressionModel.predict(numericalX)
        accuracyDefault = targetMetric(y, yDefault)
        self.targetMetricImprovement_ = (accuracyDefault - accuracyModel) / accuracyDefault
        
        return self.targetMetricImprovement_
            
    def targetMetricImprovement(self, X, y, targetMetric=root_mean_squared_error):
        '''
        It calculates the difference in the target metric of 
        the whole pattern-aided model and the default regression model.
        '''
        ## Predict using the local patterns + the default model
        numericalX = X[self.localModels[0].numericalHeadersForPrediction]
        yModel = self.predict(X)       
        accuracyModel = targetMetric(y, yModel)
        
        ## Predict using only the default model
        yDefault = self.referenceRegressionModel.predict(numericalX)
        accuracyDefault = targetMetric(y, yDefault)
        self.targetMetricImprovement_ = (accuracyDefault - accuracyModel) / accuracyDefault
        
        return self.targetMetricImprovement_
    
    def parseIntervals(self, intervalStrings):
        parsedIntervals = []
        for intervalStr in intervalStrings :
            m = re.search('\((-?inf|[\.0-9]+), (-?inf|[\.0-9]+)\)', intervalStr)
            if m is not None : 
                floor = m.group(1)
                ceiling = m.group(2)
                parsedIntervals.append((float(floor), float(ceiling)))
        
        return parsedIntervals
     
         
    def discretize(self, X):
        trainingData = self.getDataUsedForLearning()
        discrCols = [col for col in trainingData.columns if col.startswith('discretized_')]
        
        ## No need to discretize
        if all(elem in X.columns for elem in discrCols) :
            return X
        
        Xdisc = trainingData[discrCols]
        Xresult = X.copy()
        for col in discrCols : 
            discrVals = list(set(Xdisc[col]))
            originalColName = col.replace('discretized_', '')
            parsedIntervals = self.parseIntervals(discrVals)
            Xresult[col] = X[originalColName].apply(discretizeValue, args = (parsedIntervals, discrVals))            
        
        #print(Xresult)        
        return Xresult
     
     
    def predict(self, X) :        
        '''
        It takes a data frame containing numerical and categorical attributes and 
        makes a prediction for each of the data points.
        '''
        Xd = self.discretize(X)
        predictions = []
        for index, row in Xd.iterrows():                
            prediction = nan
            weightFactor = 0.0
            matchedModels = []
            for i in range(len(self.localModels)) :
                model = self.localModels[i]                
                term = model.predict(row)
                if not isnan(term) :
                    matchedModels.append(model.pattern)
                    if isnan(prediction) :
                        prediction = 0.0
                        
                    prediction += self.weights[i] * term
                    weightFactor += self.weights[i]
                                
            if isnan(prediction) :
                numericalVars = self.localModels[0].numericalHeadersForPrediction                
                dataRow = pd.DataFrame.from_records([tuple([row[att] for att in numericalVars])], 
                                            columns=numericalVars)
                if self.defaultRegressionModel is not None :
                    prediction = self.defaultRegressionModel.predict(dataRow)[0]
                else :
                    prediction = self.referenceRegressionModel.predict(dataRow)[0]
            else :
                prediction = prediction / weightFactor
            
            predictions.append(prediction)    
        
        return pd.Series(predictions)
    
    def predictWithIndexHelp(self, X):
        '''
        This assumes that we have an index row->matched models to
        speed up the calculation of the prediction. This method should be used
        only to predict on subsets of the training data.
        '''
        predictions = []
        numericalVars = self.localModels[0].numericalHeadersForPrediction
        for index, row in X.iterrows():                
            prediction = 0.0
            weightFactor = 0.0
            matchedModels = self.data2Models[index]
                        
            if len(matchedModels) > 0 :
                for model in matchedModels :                    
                    weight = self.models2Weights[model.pattern[0]]
                    prediction += weight * model.evaluate(row)
                    weightFactor += weight
            else :            
                weightFactor = 1.0
                dataRow = pd.DataFrame.from_records([tuple([row[att] for att in numericalVars])], 
                                            columns=numericalVars)
                if self.defaultRegressionModel is not None :
                    prediction = self.defaultRegressionModel.predict(dataRow)[0]
                else :
                    prediction = self.referenceRegressionModel.predict(dataRow)[0]
            
            prediction = prediction / weightFactor            
            predictions.append(prediction)    
        
        return pd.Series(predictions)
    
    def predictWithIndexHelpImproved(self, X):
        numericalVars = self.localModels[0].numericalHeadersForPrediction
        ## We build the masks for each of the models
        masks = [ model.matchesMask(X) for model in self.localModels ]
        rawComponents = [X[masks[i]][numericalVars] for i in range(len(masks))]
        components = [ self.localModels[i].localRegressionModel.predict(rawComponents[i]) for i in range(len(masks)) ]
        for i in range(len(components)) :
            complementIndex = X[~masks[i]].index
            components[i] = pd.Series(components[i], index=rawComponents[i].index)
            components[i] = components[i].append(pd.Series(0, index=complementIndex))
        
        defaultMask = pd.Series([len(self.data2Models[index]) == 0 for index, row in X.iterrows()], X.index)
        considerDefaultMask = any(defaultMask)
        if considerDefaultMask :
            ## Take care of the points that are not covered by the local models
            masks.append(defaultMask)
            rX = X[defaultMask][numericalVars]
            if self.defaultRegressionModel is not None :
                defaultComponent = pd.Series(self.defaultRegressionModel.predict(rX), index=rX.index)
            else:
                defaultComponent = pd.Series(self.referenceRegressionModel.predict(rX), index=rX.index)
            
            defaultComponent = defaultComponent.append(pd.Series(0, index=X[~defaultMask].index))
            components.append(defaultComponent)
            
            
        weights = [self.models2Weights[model.pattern[0]] for model in self.localModels]
        if considerDefaultMask :
            weights.append(1.0)
            
        weightNormVectors = [pd.Series(weights[i], index=masks[i].index) * masks[i] for i in range(len(components))]
        normalization = sum(weightNormVectors)
        weightedComponents = [(weightNormVectors[i] * components[i]) / normalization for i in range(len(weights))]
        return sum(weightedComponents)
    
    
    def addLocalModel(self, newModel):
        '''
        It returns a new regression function that is a copy of the calling object
        plus the new local model.
        @return: A new regression function containing the original local models plus 
        the new model, or None if the new model is already in the function
        '''
        if newModel in self.localModels :
            return None
        
        newLocalModels = self.localModels.copy()
        newLocalModels.append(newModel)
        newFunction = PatternBasedRegressionFunction(newLocalModels, self.referenceRegressionModel)
        return newFunction
    
    def replaceLocalModel(self, oldModel, replacementModel):
        '''
        It returns a new regression function that is a copy of the calling object
        that replaces the old model with the replacement model
        '''
        newLocalModels = self.localModels.copy()
        for i in range(len(newLocalModels)) :
            if newLocalModels[i] == oldModel :
                newLocalModels[i] = replacementModel
                return PatternBasedRegressionFunction(newLocalModels, self.referenceRegressionModel) 
        
        ## If the old model was not in the list of models, return None
        ## This is a pathological situation
        return None
    
    def __str__(self):
        result = '{'
        
        for model in self.localModels :
            result += str(model) 
        
        if self.defaultRegressionModel is not None :
            result += ', defaultModel: ' + regressionModelToStr(self.localModels[0].localDataForRegression, 
                                                                self.defaultRegressionModel)        
        else :
            result += ', referenceModel: ' + regressionModelToStr(self.localModels[0].localDataForRegression, 
                                                                self.referenceRegressionModel)        
        result += '}'
        
        return result
    
    def getPatterns(self): 
        patterns = []
        for model in self.localModels :
            patterns.append(model.pattern[0])
        
        return patterns


class LocalRegressionModel(object):
    
    def __init__(self, data, pattern, targetVariable, defaultRegressionModel, regressionMethod):
        self.originalData = data
        self.pattern = pattern
        self.localData = queryDataset(self.originalData, self.pattern[0])
        
        self.numericalHeaders = self.localData.select_dtypes(include=[np.number]).columns.tolist()
        self.numericalHeadersForPrediction = self.numericalHeaders.copy()
        self.categoricalVariables = self.localData.select_dtypes(include=[np.object]).columns.tolist()
        self.regressionMethod = regressionMethod
        self.targetVariable = targetVariable
        self.targetData = self.localData[self.targetVariable] 
        self.numericalHeadersForPrediction.remove(self.targetVariable)
        
        if 'residuals_' + self.targetVariable in self.numericalHeadersForPrediction :
            self.numericalHeadersForPrediction.remove('residuals_' + self.targetVariable)
        if 'error_class_' + self.targetVariable in self.numericalHeadersForPrediction :
            self.numericalHeadersForPrediction.remove('error_class_' + self.targetVariable)            

        self.localDataForRegression = self.localData[self.numericalHeadersForPrediction]        
        self.localRegressionModel = self.regressionMethod().fit(self.localDataForRegression, self.targetData)        
        self.defaultRegressionModel = defaultRegressionModel


    def getRawARRValues(self):
        ## First, calculate the total residuals of the default model in the 
        ## items that match the pattern.
        residualsVarName = 'residuals_' + self.targetVariable        
        if residualsVarName in self.localData.columns : 
            ## Get the residuals from what we already computed
            residualsDefaultModel = np.sum(self.localData[residualsVarName])
        else:
            yPredicted = self.defaultRegressionModel.predict(self.localDataForRegression)
            residualsDefaultModel = np.sum(np.absolute(yPredicted - self.targetData))

        ## Now calculate the residuals of the local model
        yPredicted = self.localRegressionModel.predict(self.localDataForRegression)
        residualsLocalModel = np.sum(np.absolute(yPredicted - self.targetData))
        
        
        return residualsDefaultModel, residualsLocalModel

    def getARR(self):
        '''
        It calculates the average residual reduction of the local regression model 
        on the data subset defined by the corresponding pattern on the target variable.
        '''
        ## Support of the local pattern
        if not hasattr(self, 'ARR_') :
            support = float(len(self.localData))
            residualsDefaultModel, residualsLocalModel = self.getRawARRValues()
            self.ARR_ = (residualsDefaultModel - residualsLocalModel) / support
        
        return self.ARR_
    
    
    def getARRReduction(self):
        '''
        It calculates the reduction in ARR provided by the local model w.r.t the default 
        regression model.
        '''
        residualsDefaultModel, residualsLocalModel = self.getRawARRValues()
        return (residualsDefaultModel - residualsLocalModel) / residualsDefaultModel

        
    def __str__(self):
        result = '{Pattern: ' + str(self.pattern[0]) + ', support: ' + str(self.pattern[1][1])
        result = result + ', relative-risk: ' + str(self.pattern[1][0])
        result = result + ', localModel: ' + regressionModelToStr(self.localDataForRegression, self.localRegressionModel)
        result = result + ', defaultModel: ' + regressionModelToStr(self.localDataForRegression, self.defaultRegressionModel)  + '}'        
        return result
    
    def matches(self, x):
        '''
        It determines if the given data point matches the calling local model.
        '''
        for attrValueTuple in self.pattern[0] :
            attribute = attrValueTuple[0]
            value = attrValueTuple[1]
            if x[attribute] != value :
                return False
        
        return True
    
    def matchesMask(self, X):
        return pd.Series([self.matches(row) for index, row in X.iterrows()], X.index)
    
    def evaluate(self, x):
        arr = pd.DataFrame.from_records([tuple([x[att] for att in self.numericalHeadersForPrediction])], 
                                        columns=self.numericalHeadersForPrediction)
        value = self.localRegressionModel.predict(arr)
        return value[0]
    
    def predict(self, x):
        '''
        It predicts the value of a single data point.
        @return: The predicted value or nan if the local model is not applicable to this data point, that is,
        the data point does not match the model's pattern.
        '''
        if self.matches(x) :            
            return self.evaluate(x)
        else :
            return nan

class PatternBasedRegression(object):
    '''
    classdocs
    '''


    def __init__(self, regressionMethod=linear_model.LinearRegression, 
                 defaultRegressionFunction=None,
                 overlapTolerance=0.9, errorTolerance=0.45, minSupport=0.02,
                 minDiscriminativeRatio=1, enableDiscretization=True, forceDiscretization=False, 
                 arrMinImprovement=0.01):
        '''
        It initializes the regression object.
        @param regressionMethod. Type of function used to learn regression models on subsets of
        the data. It is required to be an object that implements two methods: fit(X, y) and 
        predict(X).        
        @param defaultRegressionFunction. The quality of the returned patterns is compared
        w.r.t this default model. By default is None, which means that the object will 
        learn the default function using the provided regression method.
        @param overlapTolerance. It is a number in (0, 1). It determines the minimum 
        overlap between two local models (in terms of covered data points) so that 
        they are considered redundant. In that case, the model with the smallest average
        residual reduction is preferred.
        @param errorTolerance. It is a number in (0, 1). Assuming the data
        points have been sorted in ascending order by the model's prediction error, 
        this factor determines the proportion of the model's total error 
        covered by the points in the low-error class.
        @param minSupport. The minimal support (number between 0 and 1) of the patterns
        used for pattern based regression.
        @param minDiscriminativeRatio. Threshold on the support ratio of a patterns in the large 
        error (LE) and small error classes.
        @param enableDiscretization. If true, the object will try to discretize the numerical variables
        to improve the quality of the local models. The discretization creates new categorical 
        variables in the data.
        @param forceDiscretization. If true, the object will force a (potentially suboptimal) discretization 
        of the numerical variables. This parameter does not have any effect if enableDiscretization=False.
        @param arrMinImprovement. Patterns that offer an average residual reduction (arr) 
        smaller than this value will be ignored.        
        '''
        self.overlapTolerance = overlapTolerance
        self.errorTolerance = errorTolerance
        self.regressionMethod = regressionMethod
        self.defaultRegressionFunction = defaultRegressionFunction
        self.minSupport = minSupport
        self.minDiscriminativeRatio = minDiscriminativeRatio
        self.enableDiscretization = enableDiscretization
        self.forceDiscretization = forceDiscretization
        self.arrMinImprovement = arrMinImprovement
     
    
    def overlap(self, localModel1, localModel2):
        '''
        It computes the jaccard coefficient between the set of data points covered
        by the corresponding local models.
        '''
        idx1 = set(localModel1.localData.index.values)
        idx2 = set(localModel2.localData.index.values)

        union = set(idx1)
        union |= idx2         
        intersection = idx1 & idx2
        
        return float(len(intersection)) / float(len(union)) 
     
     
    def filterRedundantModels(self, inputLocalModels):
        '''
        It prunes the input list of local models by removing those with a small
        average residual reduction that overlap too much with other local models.
        '''
        redundanceDictionary = {}
        pattern2Model = {}
        pattern2ARR = {}
        for model in inputLocalModels :
            redundanceDictionary[model.pattern] = False
            pattern2Model[model.pattern] = model
            pattern2ARR[model.pattern] = model.getARR()
        
            
        for i in range(len(inputLocalModels)) :            
            if redundanceDictionary[inputLocalModels[i].pattern] :
                continue
            
            for j in range(i + 1, len(inputLocalModels)) :
                if redundanceDictionary[inputLocalModels[i].pattern] :
                    break
                
                if redundanceDictionary[inputLocalModels[j].pattern] :
                    continue
                
                overlap = self.overlap(inputLocalModels[i], inputLocalModels[j])
                if overlap >= self.overlapTolerance :
                    if pattern2ARR[inputLocalModels[i].pattern] > pattern2ARR[inputLocalModels[j].pattern] :
                        redundanceDictionary[inputLocalModels[j].pattern] = True
                    else :
                        redundanceDictionary[inputLocalModels[i].pattern] = True
        
        for patternModel in redundanceDictionary.keys() :
            if redundanceDictionary[patternModel] :
                inputLocalModels.remove(pattern2Model[patternModel])
        
    
    def discretize(self, data, targetColumn, numericalAttributes):
        if self.forceDiscretization :
            mdlp = MDLP(min_depth=1)
        else:
            mdlp = MDLP()
        convData = mdlp.fit_transform(data[numericalAttributes], targetColumn)
        discretizedData = pd.DataFrame({})
        ## Now interpret the discretization
        for i in range(convData.shape[1]) :
            discretizedValues = [str(x) for x in mdlp.cat2intervals(convData, i)]
            distinctValues = set(discretizedValues)
            ## Ignore the attributes where there was no discretization
            if len(distinctValues) > 1 :
                discretizedData['discretized_' + numericalAttributes[i]] = discretizedValues
        
        return discretizedData
    
    def fitWithoutFiltering(self, X, targetVariableName) :
        '''
        It learns a set of local models using the assigned regression model at object
        construction time. The local models are based on discriminative patterns.
        The methods reports only the models whose average residual reduction is greater
        or equal than the attribute arrMinImprovement.        
        '''
        errorSplitter = ErrorBasedSplitter(X, self.errorTolerance)

        ## We get the points where the regression model makes a small error
        if self.defaultRegressionFunction is None :
            leX, seX, defaultRegressionFn = errorSplitter.split(targetVariableName, 
                                                                self.regressionMethod())
        else:
            leX, seX, defaultRegressionFn = errorSplitter.split(targetVariableName, 
                                                                self.defaultRegressionFunction)
            
        
        
        discriminantAttribute = 'error_class_' + targetVariableName
        dataForPatternMining = errorSplitter.concat(leX, seX, discriminantAttribute)
        
        # We discretize the numerical variables w.r.t to SE and LE
        discriminativeColumn = dataForPatternMining[discriminantAttribute]
        del dataForPatternMining[discriminantAttribute]        
        numericalAttributes = dataForPatternMining.select_dtypes(include=[np.number]).columns.tolist()
        numericalAttributes.remove(targetVariableName)
        numericalAttributes.remove('residuals_' + targetVariableName)        
        
        if self.enableDiscretization :
            discretizedArguments = self.discretize(dataForPatternMining, discriminativeColumn, numericalAttributes)
            dataForPatternMining = pd.concat([dataForPatternMining, discretizedArguments], axis=1)                    

        dataForPatternMining[discriminantAttribute] = discriminativeColumn

        
        ## Now we learn discriminant patterns w.r.t the categorizations
        ## SE and LE
        patternMiner = DiscriminativePatternsMiner(dataForPatternMining, discriminantAttribute)
        discriminativePatterns = patternMiner.mine(self.minSupport, 
                                                   self.minDiscriminativeRatio, 
                                                   Metric.RELATIVE_RISK, 
                                                   [1, 0])
        
        localModels = []
        for pattern in discriminativePatterns :
            localModel = LocalRegressionModel(dataForPatternMining, pattern, targetVariableName,
                                              defaultRegressionFn, self.regressionMethod)
            
            ## First filtering by ARR
            if localModel.getARRReduction() >= self.arrMinImprovement :
                localModels.append(localModel)
        
        return localModels
        
    def fit(self, X, targetVariableName):
        '''
        It learns a set of local models using the assigned regression model at object
        construction time. The local models are based on discriminative patterns. Redundant 
        models are filtered, i.e., models with low average residual reduction that cover
        the same points as other models. 
        '''
        localModels = self.fitWithoutFiltering(X, targetVariableName)
        ## Remove those models whose sets of data points overlap too much 
        self.filterRedundantModels(localModels)
        
        ## We still lack the test that checks whether the support of a pattern
        ## is smaller than the number of predictor variables.        
        return localModels
    
    def modelsAsDict(self, models): 
        result = {}
        for model in models :
            result[model.pattern[0]] = model
            
        return result