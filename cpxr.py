'''
Created on Apr 22, 2018

@author: luis
'''
import pandas as pd
import functools
import numpy as np
import sys
import csv
import time
import random
from math import sqrt
from os import path
from sklearn import linear_model
from find_local_regression_models import PatternBasedRegression
from find_local_regression_models import PatternBasedRegressionFunction
from find_local_regression_models import root_mean_squared_error
from sklearn.metrics.regression import mean_squared_error, median_absolute_error
from macpath import basename


def TRRImprovement(functionWithoutReplacement, functionWithReplacement, X, y):
    return functionWithReplacement.getTRR(X, y) - functionWithoutReplacement.getTRR(X, y)


class CPXR(object):
    '''
    It implements the CPXR algorithm from G. Dong and V. Taslimitehrani, ICDE 2016
    '''
    def __init__(self, regressionMethod=linear_model.LinearRegression, 
                 defaultRegressionFunction=None,
                 overlapTolerance=0.9, errorTolerance=0.45, minSupport=0.02,
                 minDiscriminativeRatio=1, enableDiscretization=True,
                 forceDiscretization=True, arrMinImprovement=0.01,
                 targetMetricMinImprovement=0.01, iterativeTRRMinImprovement=0.001,
                 optimizationMetric=root_mean_squared_error):
        '''
        It initializes the CPXR regression object.
        @param regressionMethod. Type of function used to learn regression models on subsets of
        the data. It is required to be an object that implements two methods: fit(X, y) and 
        predict(X).
        @param defaultRegressionFunction. The quality of the returned patterns is compared
        w.r.t this default model. By default is None, which means that the object will 
        learn the default function using the provided regression method. This attribute is mainly
        used for debugging purposes to guarantee that we can compare against a fixed model.
        @param overlapTolerance. It is a number in (0, 1). It determines the minimum 
        overlap between two local models (in terms of covered data points) so that 
        they are considered redundant. In that case, the model with the smallest average
        residual reduction is preferred.
        @param errorTolerance. It is a number in (0, 1). Assuming the data
        points have been sorted in ascending order by the model's prediction error, 
        this factor determines the proportion of the model's total error 
        covered by the points in the low-error class.
        @param minSupport. The minimal support (number between 0 and 1) of the patterns
        used for pattern based regression.
        @param minDiscriminativeRatio. Threshold on the support ratio of a patterns in the large 
        error (LE) and small error classes.
        @param enableDiscretization. If true, the object will try to discretize the numerical variables
        in the dataset in order to improve the quality of the local models. The discretization creates 
        new categorical variables in the data.
        @param forceDiscretization. If true, the object will force a (potentially suboptimal) discretization 
        of the numerical variables. This parameter does not have any effect if enableDiscretization=False.        
        @param arrMinImprovement. Patterns that offer an average residual reduction (arr) 
        smaller than this value will be ignored.   
        @param targetMetricMinImprovement. If an iteration of the algorithm yields an improvement higher
        than this value, the algorithm stops trying and returns the models found so far.     
        @param iterativeMinImprovement. Given a candidate set of models, the algorithm will 
        test the addition of new models to this set while the improvement (in terms of arr) is greater
        than this value.
        @param optimizationMetric. The error metric that all regression models try to optimize for.
        By default it is the root_mean_squared_error.
        '''
        self.patternBasedRegressor = PatternBasedRegression(regressionMethod, defaultRegressionFunction, overlapTolerance, 
                                                            errorTolerance, minSupport, minDiscriminativeRatio,
                                                            enableDiscretization, forceDiscretization, arrMinImprovement)
        self.iterativeTRRMinImprovement = iterativeTRRMinImprovement
        self.targetMetricMinImprovement = targetMetricMinImprovement
        self.defaultRegressionFunction = defaultRegressionFunction
        self.optimizationMetric = optimizationMetric
    
    
    def iterativeImprovement(self, targetCompositeModel, remainingModels, X, y):
        '''
        Given a composite model and the list of remaining local models, it modifies
        the composite model by replacing its components with models in the list of 
        remaining local models. The process is done iteratively while the improvement
        in total residual reduction (TRR) is greater than the given threshold 
        (variable iterativeTRRMinImprovement in the constructor)
        '''
        ## For each pattern P in the composite model, find its best replacement Q, i.e.,
        ## another pattern such that the TRR improvement of replacing P by Q is maximal
        bestReplacement = targetCompositeModel
        improvement = 1.0
        ## While the improvement is big enough
        while improvement > self.iterativeTRRMinImprovement :        
            bestReplacements = []            
            for model in bestReplacement.localModels :
                ## For a particular model, compute the improvement of replacing it
                ## by each of the remaining models (variable localModels)    
                #startTime = time.process_time()
                candidateReplacements = [bestReplacement.replaceLocalModel(model, x) for x in remainingModels]
                #print('Replacing the candidates took', (time.process_time() - startTime))
                #startTime = time.process_time()
                for candidateReplacement in candidateReplacements :
                    candidateReplacement.TRRImprovement = TRRImprovement(bestReplacement, candidateReplacement, X, y)
                
                #print('Calculating their TRR improvement took', (time.process_time() - startTime))
                candidateReplacements.sort(key=lambda x : x.TRRImprovement, reverse=True)                
                bestReplacementForModel = candidateReplacements[0]
                ## Get the best replacement couple for this particular model
                bestReplacements.append((model, bestReplacementForModel))
        
            ## Now get the best couple ever ^_^
            bestReplacements.sort(key=lambda couple : couple[1].TRRImprovement, reverse=True)      
            bestReplacementCouple = bestReplacements[0]
            bestReplacementCandidate = bestReplacementCouple[1]        
            
            improvement = bestReplacementCandidate.TRRImprovement             
            if improvement > 0.0 :
                ## We proceed with the replacement
                bestReplacement = bestReplacementCandidate
                ## Remove the model that was sacrificed in this replacement
                if bestReplacementCouple[0] in remainingModels :
                    remainingModels.remove(bestReplacementCouple[0])
                    
        return bestReplacement
        
    def fit1(self, X, targetVariableName) :
        '''
        It learns a pattern-aided regression model for the target variable contained in the input
        dataset X. The model consists of a set of local regression models defined on subsets of the data
        plus a default regression model used for the data points not covered by any pattern.
        '''
        print('Learning the patterns')     
        startTime = time.process_time()           
        models = self.patternBasedRegressor.fit(X, targetVariableName)
        print('Learning the patterns took', (time.process_time() - startTime), 'seconds')

        y = X[targetVariableName]  
        del X[targetVariableName]
              
        #Sort the models by ARR
        models.sort(key=lambda model : model.getARR(), reverse=True)
        startTime = time.process_time()         
        if self.defaultRegressionFunction is None:        
            #Initialize the regression function with the model having the highest ARR
            regressionFunction = PatternBasedRegressionFunction([models[0]], 
                                                                models[0].defaultRegressionModel)
        else :
            regressionFunction = PatternBasedRegressionFunction([models[0]], 
                                                                self.defaultRegressionFunction)
        ## Remove the model with the highest ARR
        models.remove(models[0])                
        ## Restore the input data
        X[targetVariableName] = y   
        
        ## Get the new data containing the potentially discretized variables
        newX = regressionFunction.getDataUsedForLearning()
        ## Remove unnecessary columns
        del newX[targetVariableName]
        del newX['residuals_' + targetVariableName]
        del newX['error_class_' + targetVariableName]                     

        ## Now try to refine the regression function
        compositeModels = [regressionFunction.addLocalModel(x) for x in models]
        while len(compositeModels) > 0 :  
            #print('New outer iteration. Sorting the models by target metric')    
            #startTime = time.process_time()
            compositeModels.sort(key=lambda m : functools.partial(PatternBasedRegressionFunction.targetMetricImprovementWithIndexHelp, m)(newX, y, self.optimizationMetric),
                                 reverse=True)
            #print('Sorting the models by target metric took', (time.process_time() - startTime), 'seconds')
            
            
            compositeModel = compositeModels[0]    
            if compositeModel.targetMetricImprovement_ <= 0.0 :
                break
            
            print('Reduction', compositeModel.targetMetricImprovement_, compositeModel)
            print('Running iterative improvement')
            startTime = time.process_time()         
            compositeModel = self.iterativeImprovement(compositeModel, models, newX, y)        
            print('Iterative improvement took', (time.process_time() - startTime), 'seconds')

            print('Calculating improvement of improved model')
            startTime = time.process_time() 
            ## See if the data has been already calculated
            if hasattr(regressionFunction, 'targetMetricImprovement_') :
                previousImprovement = regressionFunction.targetMetricImprovement_
            else:
                previousImprovement = regressionFunction.targetMetricImprovement(newX, y, self.optimizationMetric)
            
            deltaImprovement = compositeModel.targetMetricImprovement(newX, y, self.optimizationMetric) - \
                previousImprovement 
                
            print('Calculating improvement took', (time.process_time() - startTime), 'seconds')
            print('delta improvement', deltaImprovement, 'of going from', regressionFunction.getPatterns(), 'to', compositeModel.getPatterns())

            if deltaImprovement < self.targetMetricMinImprovement :
                break
            
            regressionFunction = compositeModel
            
            # Recalculate the new models
            compositeModels = []
            for model in models :
                newModel = regressionFunction.addLocalModel(model)
                if newModel is not None :
                    compositeModels.append(newModel)

               
        regressionLe = self.patternBasedRegressor.regressionMethod()
        ## The default regression model is now learnt from those points not covered by any pattern
        uncoveredX, uncoveredY = regressionFunction.getUncoveredData(newX, y)
        regressionFunction.defaultRegressionModel = regressionLe.fit(
            uncoveredX[uncoveredX.select_dtypes(include=[np.number]).columns], 
            uncoveredY)

        return regressionFunction        
    
    def fit(self, X, y):
        Xplusy = pd.concat([X, y])
        return self.fit1(Xplusy, y.columns[0])
    

def loadCSVData(dataPath):
	    '''
    It returns a pandas dataframe from a CSV file with typed headers. A typed header looks like
    header-name@type. The type can be any Python type, but the method supports the aliases "string" 
    for str and "numeric" for float.
    '''
    with open(dataPath, 'r') as fdata :
        csvReader = csv.reader(fdata)
        headers = next(csvReader)
        actualHeaders = []
        for header in headers :
            headerParts = header.split('@')
            actualHeader = headerParts[0]
            headerType = headerParts[1]
            if headerType == 'string' :
                headerType = str
            elif headerType == 'numeric' :
                headerType = float
            else :
                headerType = eval(type)

            actualHeaders.append((actualHeader, headerType))
        
        data = []
        lineN = 1
        for row in csvReader :
            newRow = []
            for i in range(len(row)) :
                dataType = actualHeaders[i][1]
                try :
                    newRow.append(dataType(row[i]))
                except ValueError as e :
                    if dataType == float :
                        newRow.append(float('nan'))
                    else :
                        print('Error at converting "', row[i], '" to', dataType, 'in column', i, 'in line', lineN)    
            
            lineN += 1    
            data.append(tuple(newRow))
        
        cols = [x[0] for x in actualHeaders]
        return pd.DataFrame.from_records(data, columns=cols)


def getVineCols():
    return ['cepage', 'commune_parcelle', 'date_obs', 'day', 'insee_parcelle', 
            'latitude', 'longitude', 'month', 'nom_parcelle', 'semaine_obs', 'stade_pheno', 
            'timestamp', 'year', 'ETP_sum_4w', 'HR_sum_4w', 'RG_sum_4w', 'RR_dry_days', 'RR_rainy_days', 
            'RR_sum', 'TN_above_11c', 'TX_above_25c', 'T_growing_day_degree', 'T_sum', 'T_sum_amplitude', 'V10_sum_4w',
            'mfi']

if __name__ == "__main__":
    if len(sys.argv) < 3 or len(sys.argv) % 2 != 1 :
        print('Not enough arguments. The program expects at least one path to a dataset and a target variable', 
              file=sys.stderr)
        print('./cpxr.py csv-dataset-1 target-variable-1 csv-dataset-2 target-variable-2 ...')
        sys.exit(1)
        
    ## Now read each dataset
    for metric in {'rmse' : root_mean_squared_error, 'mae': median_absolute_error}.items() :    
        print('Learning model for metric', metric[0])
        for i in range(1, len(sys.argv), 2): 
            dataPath = sys.argv[i]
            targetVariableName = sys.argv[i + 1]

            if 'example_paper_cpxr_labeled_cols' in basename(dataPath) :
                cpxrLearner = CPXR(forceDiscretization=True, optimizationMetric=metric[1])        
            else :
                cpxrLearner = CPXR(optimizationMetric=metric[1])            
                
            data = loadCSVData(dataPath)
            
            if 'bsv_vigne_maladies_epicure.csv' in basename(dataPath) :
                data = data[getVineCols()]
                
            print('Learning model for dataset', dataPath, 'for metric', metric[0])
            startTime = time.process_time()
            cpxrFn = cpxrLearner.fit1(data, targetVariableName)
            print('Learning took', (time.process_time() - startTime), 'seconds')
            print(cpxrFn)
            y = data[targetVariableName]
            
            dfl = cpxrFn.getDataUsedForLearning().copy()
            print(dfl)
            for col in dfl.columns :
                if col.startswith('discretized_') :
                    del dfl[col]
            
            cpxrRMSE = cpxrLearner.optimizationMetric(y, cpxrFn.predict(dfl))
            defaultInitialModel = cpxrFn.getReferenceRegressionFunction()
            
            defaultInitialModelRMSE = cpxrLearner.optimizationMetric(y, defaultInitialModel.predict(cpxrFn.getNumericalDataUsedForLearning()))
            reduction = (defaultInitialModelRMSE - cpxrRMSE) / defaultInitialModelRMSE
            print("Dataset:", path.basename(dataPath), ' --> ', metric[0] , 'reduction:', reduction)
