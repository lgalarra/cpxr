'''
Created on Apr 10, 2018

@author: lgalarra
'''

from enum import Enum
import math
import pandas as pd
from pandas.api.types import is_numeric_dtype
import numpy as np
import time


class Metric(Enum):
    SUPPORT = 0
    RELATIVE_RISK = 1
    ODDS_RATIO = 2


def queryDataset(dataset, notIntegerPattern):
    if len(notIntegerPattern) >= 30 :
        patternChunks = [notIntegerPattern[i:i + 30] for i in range(0, len(notIntegerPattern), 30)]
    else :
        patternChunks = [notIntegerPattern]
    
    reference = dataset
    for chunk in patternChunks :
        queryStr = pattern2QueryString(chunk)
        reference = reference.query(queryStr)

    
    return reference

   
def element2QueryString(element):
    return "".join([element[0], '=="', str(element[1]), '"'])

def item2QueryString(item, item2Elements):
    element = item2Elements[item]
    return element[0] + '==' + str(item)

def integerPattern2QueryString(pattern, items2Elements):
    query = item2QueryString(pattern[0], items2Elements)
    for i in range(1, len(pattern)) :
        element = pattern[i]
        query = query + ' & ' + item2QueryString(element, items2Elements)
     
    return query

def pattern2QueryString(pattern):
    ## Initialize the query
    query = element2QueryString(pattern[0])
    for i in range(1, len(pattern)) :
        element = pattern[i]
        query = query + ' & ' + element2QueryString(element)
     
    return query


class DiscriminativePatternsMiner(object):
    '''
    The instances of this class mine discriminative patterns from a dataframe
    w.r.t a class, which is given as an attribute in the data. Only non-numerical
    attributes are considered for the mining.
    '''


    def __init__(self, data, referenceClass):
        '''
        Discriminative pattern mining initialization. The reference class is an 
        attribute in the data w.r.t which this class mines the discriminative 
        patterns.
        '''
        self.referenceClass = referenceClass
        
        if self.referenceClass not in data.columns :
            raise ValueError(referenceClass + ' is not a field in the data.')
        
        dataHeaders = list(data.select_dtypes(exclude=[np.number]))
        if self.referenceClass not in dataHeaders :
            dataHeaders.append(self.referenceClass)
        
        ## Get the data + the reference class
        self.data = data[dataHeaders]
        
        ## Store the attributes without the reference class
        self.attributes = dataHeaders
        self.attributes.remove(referenceClass)
        
        ## Transform the data into an integer space for faster computation
        self.transformedData, self.values2Items, self.items2Values = self.transformData2NumericalSpace()

    def transformData2NumericalSpace(self):
        '''
        Assigns a unique integer to each attribute=value pair occurring 
        in the data.
        '''
        dictionary = {}
        invDictionary = {}
        transformedData = {}
        currentId = 1
        for attribute in self.attributes :
            transformedData[attribute] = []
            for index, record in self.data.iterrows():  
                element = (attribute, record[attribute])
                if element not in dictionary :
                    dictionary[element] = currentId
                    invDictionary[currentId] = element
                    currentId += 1
                
                transformedData[attribute].append(dictionary[element])
        
        return pd.DataFrame(transformedData), dictionary, invDictionary
        
        
    def getAttributesForRecord(self, record, output) :   
        for attribute in self.attributes :
            output.add(record[attribute])

   
    def patternGetSupportSet(self, referenceData, pattern):
        '''
        It returns the support of a pattern in the data. The pattern
        is provided as a list of integers (each one representing a condition
        of the form attribute=value.
        '''
        ## We did data reduction, so we have to run only a part of the pattern
        applicablePattern = []
        columns2Delete = []
        for item in pattern :            
            element = self.items2Values[item]
            if element[0] in referenceData.columns :                
                applicablePattern.append(item)
                columns2Delete.append(element[0])
        
        
        if len(applicablePattern) >= 30 :
            patternChunks = [applicablePattern[i:i + 30] for i in range(0, len(applicablePattern), 30)]
        else :
            patternChunks = [applicablePattern]
        
        dataSubset = referenceData
        #print(pattern, applicablePattern, patternChunks, referenceData.columns, columns2Delete, referenceData, self.items2Values)
        for chunk in patternChunks :
            queryStr = integerPattern2QueryString(chunk, self.items2Values)
            dataSubset = dataSubset.query(queryStr)

        for column in columns2Delete :
            del dataSubset[column]    
        
        return dataSubset
        
    def isFirstParent(self, parentClosedPattern, newItem, childClosedPattern):
        indexOfNewItemInChild = childClosedPattern.index(newItem)
        return indexOfNewItemInChild == len(parentClosedPattern)
    
    def getClosure(self, supportSet):
        '''
        It returns the most expressive conjunction (closed pattern) that
        represents the given dataset. The patterns are returned as a sorted
        list. 
        '''
        createSet = set
        closure = createSet(supportSet.iloc[0].values)
                
        for i in range(1, len(supportSet)) :
            temporalSet = createSet(supportSet.iloc[i].values)
            closure = closure & temporalSet           
            
        return sorted(closure)
       
       
    def getIndexOfNextSatisfiableElementNotIn(self, closure, frequentAttributeValues, start):
        i = start + 1
        
        while i < len(frequentAttributeValues) and \
              (frequentAttributeValues[i] in closure or \
              not self.isSatisfiable(closure, frequentAttributeValues[i]))  :
            i += 1        
        
        return i if i < len(frequentAttributeValues) else -1
   
   
   
    def getFrequentItems(self, minSupport):
        '''
        It creates a total order for the elements of the form attribute=value
        that occur in the dataset.
        '''        
        sortedAttributeValues = []
        countDictionary = {}
    
        for i in range(len(self.transformedData)) :
            record = self.transformedData.iloc[i]
            for attribute in self.attributes :
                if attribute in record :
                    element = record[attribute]
                    try :
                        countDictionary[element] += 1
                    except KeyError :
                        countDictionary[element] = 1
        
        for key, value in countDictionary.items() :
            if value >= minSupport :
                sortedAttributeValues.append(key)
            
        sortedAttributeValues.sort()    
        return sortedAttributeValues

   
    def denormalizeSupport(self, inputSupport) :
        outputSupport = inputSupport
        floatTypes = [float, np.float64, np.float32, np.float16]
        if type(inputSupport) in floatTypes and inputSupport <= 1.0 :            
            outputSupport = math.ceil(len(self.data) * inputSupport)
            print('Using an absolute support value of', outputSupport)
            
        return outputSupport
        
        
    def isSatisfiable(self, pattern, item):        
        '''
        It verifies whether the conjunction pattern ^ item is satisfiable, i.e.,
        item does not impose a different value on a attribute already considered
        in the pattern.
        '''
        element = self.items2Values[item]
        
        predicate = element[0]
        value = element[1]
        
        for item in pattern :
            elementInPattern = self.items2Values[item]
            if elementInPattern[0] == predicate and elementInPattern[1] != value :
                return False
        
        return True
   
    def integer2ElementPattern(self, pattern):
        result = []
        for item in pattern :
            element = self.items2Values[item]
            result.append(element)
        
        
        result.sort()        
        return tuple(result)
    
    def mineClosedPatterns(self, minSupport) :        
        '''
        It returns all closed patterns surpassing the given minimum support
        threshold. If the support is a provided as an integer or as a real 
        number greater than 1, the method, assumes it is an absolute value.
        If the value is given as a real number between 0 and 1, it is a assumed
        as a relative support.
        '''
        ## Handle of support
        if minSupport < 0 :
            raise ValueError("minSupport cannot be a negative number")
        minSupportThreshold = self.denormalizeSupport(minSupport)        
            
        ## Filtering individual items with low support
        frequentAttributeValues = self.getFrequentItems(minSupportThreshold)
        print(len(frequentAttributeValues), 'different attribute-values identified.')
        
        closedPatterns = []
        numberOfItems = len(frequentAttributeValues) 
        
        ## Check if there is an itemset that occurs in all items
        root = self.getClosure(self.transformedData)
        closedPatterns.append((tuple(root[:]), len(self.transformedData)))
        
        closureTime = 0
        transactionTime = 0
        sortFn = sorted
        for i in range(numberOfItems) :
            referenceDataStack = [self.transformedData]
            
            if frequentAttributeValues[i] in root :
                continue
            
            element = frequentAttributeValues[i]
            ## It means the new item imposes more than one condition with
            ## different values on the same attribute (support == 0)
            if not self.isSatisfiable(root, element) :
                continue
            
            pattern = root + [element]
            j = i
            while len(pattern) > len(root) :
                startTime = time.process_time()                
                # referenceDataStack[len(referenceDataStack) - 1] gets the peek of the stack
                supportSet = self.patternGetSupportSet(referenceDataStack[len(referenceDataStack) - 1], pattern)
                ## Update the reference data (dataset reduction)
                referenceDataStack.append(supportSet)
                transactionTime += (time.process_time() - startTime)
                if len(supportSet) >= minSupportThreshold :
                    startTime = time.process_time()
                    closure = sortFn(pattern + self.getClosure(supportSet))                    
                    closureTime += (time.process_time() - startTime)
                    #print('t-time=', transactionTime, 'c-time=' , closureTime)
                    ## Stop if this closed pattern is not the first parent 
                    ## This means we can skip this path
                    if not self.isFirstParent(pattern[:-1], pattern[-1], closure) :
                        pattern.pop()   
                        referenceDataStack.pop()  
                
                    else :  
                        ## Add the closure  
                        closedPatterns.append((self.integer2ElementPattern(closure[:]), len(supportSet)))
                        # And start the search from that point 
                        pattern = closure 
                else :
                    ## If not enough support, backtrack!
                    pattern.pop()    
                    referenceDataStack.pop()                 
        
                
                if pattern != root :
                    j = self.getIndexOfNextSatisfiableElementNotIn(pattern, frequentAttributeValues, j)  
                    if j != -1 :
                        pattern.append(frequentAttributeValues[j])
                    else :
                        ## We ran out of elements for this path
                        pattern = root
                        j = i
                  
        return closedPatterns
    
    def getPatternsWithRelativeRiskOverThreshold(self, closedPatterns, threshold, dataset1, dataset2):
        discriminativePatterns = []
        i = 0
        for patternData in closedPatterns :
            i += 1
            pattern = patternData[0]
            support = patternData[1]

            # Ignore the empty pattern
            if len(pattern) == 0 :
                continue
            
            supportSet1 = queryDataset(dataset1, pattern)
            supportSet2 = queryDataset(dataset2, pattern)
            
            support1 = float(len(supportSet1)) / float(len(dataset1))
            support2 = float(len(supportSet2)) / float(len(dataset2))
            
            try :                        
                relativeRiskRatioPattern = support1 / support2            
            except ZeroDivisionError :
                relativeRiskRatioPattern = float('inf')

            if relativeRiskRatioPattern >= threshold :
                discriminativePatterns.append((pattern, (relativeRiskRatioPattern, support)))
            
        return discriminativePatterns        
    
    def getPatternsWithOddsRatioOverThreshold(self, closedPatterns, threshold, dataset1, dataset2):
        discriminativePatterns = []
        
        for patternData in closedPatterns :
            pattern = patternData[0]
            support = patternData[1]
            
            if len(pattern) == 0 :
                continue

            supportSet1 = queryDataset(dataset1, pattern)
            supportSet2 = queryDataset(dataset2, pattern)
            
            ratio1 = float(len(supportSet1)) / float(len(dataset1) - len(supportSet1))
            ratio2 = float(len(supportSet2)) / float(len(dataset2) - len(supportSet2))
            
            try :                        
                oddsRatioPattern = ratio1 / ratio2            
            except ZeroDivisionError :
                oddsRatioPattern = float('inf')

            if oddsRatioPattern >= threshold :
                discriminativePatterns.append((pattern, (oddsRatioPattern, support)))
            
        return discriminativePatterns
    
    def mineDiscriminativePatterns(self, closedPatterns, threshold, metric=Metric.RELATIVE_RISK, classValues=None):    
        ## For each pattern look at its support in the classes
        ## We assume that the target class has only two values
        discriminativePatterns = []
        if classValues is None :
            classValues = self.data[self.referenceClass].unique()
                
        if len(classValues) != 2 :
            raise ValueError('The target class must have only' +
            'two different values. The following were found:', classValues)
            
        print('Showing discriminative patterns w.r.t', self.referenceClass, '=', classValues)

        if is_numeric_dtype(self.data.dtypes[self.referenceClass]) :
            qStr1 = self.referenceClass + "==" + str(classValues[0])
            qStr2 = self.referenceClass + "==" + str(classValues[1])
        else :
            qStr1 = self.referenceClass + "=='" + classValues[0] + "'"
            qStr2 = self.referenceClass + "=='" + classValues[1] + "'"
            
        dataset1 = self.data.query(qStr1)  
        dataset2 = self.data.query(qStr2)        
        
        if metric == Metric.RELATIVE_RISK :
            discriminativePatterns = self.getPatternsWithRelativeRiskOverThreshold(closedPatterns,
                                                                                    threshold, dataset1, dataset2)
        elif metric == Metric.ODDS_RATIO :
            discriminativePatterns = self.getPatternsWithOddsRatioOverThreshold(closedPatterns, 
                                                                                threshold, dataset1, dataset2)
                
        return discriminativePatterns
        
    def mine(self, minSupport, minDiscriminativeRatio, metric=Metric.RELATIVE_RISK, classValues=None):
        '''
        It mines all discriminant patterns that surpass the given support threshold and 
        discriminative threshold w.r.t the reference class. The discriminative threshold depends
        on the selected metric. The method supports the relative-risk and odds-ratio.
        '''
        ## First mine closed patterns
        closedPatterns = self.mineClosedPatterns(minSupport)
        ## Now filter those that are discriminative
        return self.mineDiscriminativePatterns(closedPatterns, minDiscriminativeRatio, metric, classValues)